'use strict';
var Connector = require('loopback-connector').Connector;
var util = require('util');
var assert = require('assert');
var _ = require('lodash');
var azure = require('azure-storage');
var mime = require('mime-types');
const { PassThrough } = require('stream');

util.inherits(AzurestorageConnector, Connector);
function AzurestorageConnector(settings, dataSource) {
  Connector.call(this, 'azurestorage', settings);
  this.dataSource = dataSource;
};

const create = function(filename, contents) {
  return new Promise((resolve, reject) => {
    var blobService = azure.createBlobService(this.settings.account, this.settings.key);

    var options = {
      contentSettings: {
        contentType: mime.lookup(filename)
      }
    }

    blobService.createBlockBlobFromText('cdn', filename, contents, options, function(error, result, response) {
      if (!error) {
        // File uploaded
        resolve();
      } else {
        reject(error);
      }
    });
  });
}


const get = function(filename) {
  return new Promise((resolve, reject) => {
    var blobService = azure.createBlobService(this.settings.account, this.settings.key);

    var data = [];
    var stream = new PassThrough();
    stream.on('data', (d) => {
      data.push(d);
    });

    blobService.getBlobToStream('cdn', filename, stream, function(error, result, response) {
      if (!error) {
        // File uploaded
        // var merged = [].concat.apply([], data);
        var buf = Buffer.concat(data);
        resolve({
          data: buf
        });
      } else {
        reject(error);
      }
    });
  });
}

exports.initialize = function initializeDataSource(dataSource, callback) {
  const settings = dataSource.settings;

  // Check settings defined...
  assert(settings.account, 'No account provided');
  assert(settings.key, 'No key provided');

  // Initialize
  dataSource.connector = new AzurestorageConnector(settings, dataSource);
  dataSource.ObjectID = 'azurestorage';
  dataSource.test = function() { console.log('success!'); };
  dataSource.create = create;
  dataSource.get = get;

  // Connect
  if (callback) {
    dataSource.connect(callback);
  }
};